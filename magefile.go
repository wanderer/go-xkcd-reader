//go:build mage

package main

import (
	"github.com/magefile/mage/sh"
)

// Run runs go mod download and then runs the binary.
func Run() error {
	if err := sh.Run("go", "mod", "download"); err != nil {
		return err
	}
	return sh.Run("go", "run", "-p", "2", "./...")
}

// Build runs go mod download and then builds the binary.
func Build() error {
	if err := sh.Run("go", "mod", "download"); err != nil {
		return err
	}
	return sh.Run("go", "build", "-p", "2", "./...")
}

// Test runs go mod download and then runs tests.
func Test() error {
	if err := sh.Run("go", "mod", "download"); err != nil {
		return err
	}
	return sh.Run("go", "test", "./...")
}
