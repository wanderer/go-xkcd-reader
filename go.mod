module git.dotya.ml/wanderer/go-xkcd-reader

go 1.17

require (
	github.com/diamondburned/gotk4/pkg v0.0.0-20220408070453-08962439fbbc
	github.com/magefile/mage v1.13.0
)

require (
	go4.org/unsafe/assume-no-moving-gc v0.0.0-20211027215541-db492cf91b37 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
)
