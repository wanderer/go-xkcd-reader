let
  sources = import ./sources.nix;
  goPkgs = import ./pkgs.nix { useFetched = true; };
  pkgs   = import ./pkgs.nix {};
  shell = import "shell.nix" {
    inherit pkgs;
  };
in
with pkgs;
mkShell {
  buildInputs = with pkgs; [
    gtk4
    glib
    gdk-pixbuf
    gobject-introspection
    goPkgs.go
    goPkgs.gopls
    # go
    pkgconfig
  ];
  CGO_ENABLED  = "1";
  CGO_CFLAGS   = "-g2 -Og -fno-plt -Wall -march=native -mtune=native -pipe -fdiagnostics-show-location=once";
  CGO_CXXFLAGS = "-g2 -Og -fno-plt -Wall -march=native -mtune=native -pipe -fdiagnostics-show-location=once";
  CGO_FFLAGS   = "-g2 -Og -fno-plt -Wall -march=native -mtune=native -pipe -fdiagnostics-show-location=once";
  # CGO_LDFLAGS  = "-g2 -Og -static";
  CGO_LDFLAGS  = "-g2 -Og";
  CCACHE_disabled = "false";
}

# vim: set ts=2 bs=2
