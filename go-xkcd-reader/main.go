package main

import (
	_ "embed"
	"os"

	"github.com/diamondburned/gotk4/pkg/gtk/v4"
)

//go:embed app.ui
var uiXML string

func main() {
	app := gtk.NewApplication("ml.dotya.git.wanderer.go-xkcd-reader", 0)

	app.ConnectActivate(func() { activate(app) })

	if code := app.Run(os.Args); code > 0 {
		os.Exit(code)
	}
}

func activate(app *gtk.Application) {
	// the builder contains all of the pre-defined ui objects
	builder := gtk.NewBuilderFromString(uiXML, len(uiXML))

	// construct the top-level window and set title
	window := builder.GetObject("TopLevelWindow").Cast().(*gtk.Window)
	window.SetTitle("go-xkcd-reader")

	// box is a "container" able to hold another elements
	box := builder.GetObject("Box").Cast().(*gtk.Box)
	window.SetChild(box)

	// comic origin: https://imgs.xkcd.com/comics/frankenstein_captcha.png
	const comicFilePath string = "testdata/frankenstein_captcha.png"

	const comicTitle string = "FRANKENSTEIN CAPTCHA"

	const comicTitleText string = "The distinction between a ship and a boat is a line drawn in water."

	comic := builder.GetObject("Comic").Cast().(*gtk.Image)

	// set comic image, title and "title text"
	comic.SetFromFile(comicFilePath)
	comic.SetTooltipText(comicTitleText)
	// hardcoded image size for a hardcoded comic
	comic.SetSizeRequest(273, 383)

	title := builder.GetObject("ComicTitle").Cast().(*gtk.Label)
	title.SetText(comicTitle)
	title.AddCSSClass("title")
	title.SetVisible(true)

	titletext := builder.GetObject("ComicTitletext").Cast().(*gtk.Label)
	titletext.SetText(comicTitleText)
	titletext.SetVisible(true)

	// construct and configure a headerbar
	header := builder.GetObject("HeaderBar").Cast().(*gtk.HeaderBar)

	// construct headerbar buttons
	fav := gtk.NewButtonFromIconName("emblem-favorite-symbolic")
	fav.SetTooltipText("Favourite")

	refresh := gtk.NewButtonFromIconName("view-refresh-symbolic")
	refresh.SetTooltipText("Refresh")

	about := gtk.NewButtonFromIconName("help-about-symbolic")
	about.SetTooltipText("About")

	menu := gtk.NewButtonFromIconName("view-more-symbolic")
	menu.SetTooltipText("Menu")

	// add buttons to headerbar
	header.PackStart(fav)
	header.PackStart(refresh)
	header.PackEnd(menu)
	header.PackEnd(about)

	// bind the window with out "app" and show the window
	app.AddWindow(window)
	window.Show()
}
